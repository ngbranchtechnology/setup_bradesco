if (-not (test-path "$env:ProgramFiles\7-Zip\7z.exe")) {throw "$env:ProgramFiles\7-Zip\7z.exe needed"} 
set-alias sz "$env:ProgramFiles\7-Zip\7z.exe"

$source="$PSScriptRoot\modifications\*"
$destination = "$PSScriptRoot\modifications.zip"

sz a -tzip "$destination" "$source"
